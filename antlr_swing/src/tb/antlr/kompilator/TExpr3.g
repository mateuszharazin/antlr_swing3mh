tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {  
  Integer counter = 0;
}
prog      : (e+=block | e+=expr | e+=comp | d+=decl)* -> 
            program(name={$e},declarations={$d});

block : ^(LB {enterScope();} (e+=block | e+=expr | e+=comp | d+=decl)* 
            {leaveScope();}) -> block(statement={$e},declaration={$d})
;

decl      : ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> declare(p1={$ID.text})
;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr      : ^(PLUS  e1=expr e2=expr) -> plus(p1={$e1.st},p2={$e2.st})
          | ^(MINUS e1=expr e2=expr) -> minus(p1={$e1.st},p2={$e2.st})
          | ^(MUL   e1=expr e2=expr) -> multipy(p1={$e1.st},p2={$e2.st})
          | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st},p2={$e2.st})
          | ^(PODST i1=ID   e2=expr) -> assign(p1={$ID},p2={$e2.st})
          | INT                      -> int(i={$INT.text})
          | ID                       {locals.getSymbol($ID.text);} -> id(i={$ID.text})
          | ^(IF if_processed=comp then_block=block else_block=block?) {counter++;} -> if
          (if_processed={$if_processed.st},then_block={$then_block.st},else_block={$else_block.st},no={counter.toString()})
;        

comp      : ^(EQ    e1=expr e2=expr) -> isEqual(p1={$e1.st},p2={$e2.st})
          | ^(NE    e1=expr e2=expr) -> isNotEqual(p1={$e1.st},p2={$e2.st},no={counter.toString()})
;
    